<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>tabla multiplicar</title>
    <link rel="stylesheet" href="./css.css">
</head>
<body>
    <header>
        <h1>TABLA DE MULTIPLICAR CON PHP</h1>
    </header>

    <?php
        $const=5;
        echo "<table border=3><tr align=center><td colspan=5>Tabla de multiplicar del ".$const.":</td></tr>";
        $count=1;
        while($count<=10)
        {
            if ( !($count % 2) ) {
                
                $mult=$const*$count;
                echo "<tr align=center class='gris'><td>".$const."</td><td> X </td><td>".$count."</td><td> = </td><td>".$mult."</td></tr>";
                $count++;
            }else {
                $mult=$const*$count;
                echo "<tr align=center class='grisflojo'><td>".$const."</td><td> X </td><td>".$count."</td><td> = </td><td>".$mult."</td></tr>";
                $count++;
            }
        }
        echo "</table>";
    ?>

    <footer>
        <p class="cr">2019 © Rafael González. Todos los derechos reservados</p>
    </footer>
</body>
</html>